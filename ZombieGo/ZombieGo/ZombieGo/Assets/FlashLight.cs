﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour {
	public Light light;
	public AudioClip soundOn;
	public AudioClip soundOff;
	public AudioSource audio;
	// Use this for initialization
	void Start () {
		light = GetComponent<Light> ();
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			if (light.enabled == false) {
				light.enabled = true;
				audio.clip = soundOn;
				audio.Play ();
			} else {
				light.enabled = false;
				audio.clip = soundOff;
				audio.Play ();
			}
		}	
	}
}
